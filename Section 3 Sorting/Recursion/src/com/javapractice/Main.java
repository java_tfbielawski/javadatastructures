/*  Course           : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 3        : Sorting Algorithms - Shell Sort                      *
 ** Project Name     : Factorial using recursion                            *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 01/07/2021                                           *
 ** Developer        : Tom Bielawski                                        *
 ** Time Complexity  : Depends                                             */


/*
* If num = 0, factorial = 1. Result reached.                                *
* If num != 0, set multiplier to 1                                          *
* Set factorial to 1                                                        *
* While multiplier != num, multiply factorial by multiplier and             *
* assign result to factorial.                                               *
* Add 1 to multiplier.                                                      *
* Continue until multiplier = num                                          *
* */


package com.javapractice;

//Main class
public class Main
{

    /*Iterative factorial method without recursion*/

    public static int iterativeFactorial(int num)
    {
        //Check to see if num = 0. If so, result is 1
        if (num == 0)
        {
            return 1;
        }

        //Declare and initialize variable to hold factorial
        int factorial = 1;

        //If num != 1, carry on
        for (int i = 1; i <= num; i++)
        {
            factorial *= i;

            //Print the current iteration
            System.out.println("Iterative Call: " + i);
        }

        return factorial;

    }

    /* Recursive factorial method                          *
    ** 1! = 1 * 0! | 2! = 2 * 1! | 3! = 3 * 2! | and so on *
    ** n! = n * (n - 1)!                                   *
    ** Recursive method stacks each call until all are on  *
    ** the stack. Then, calls remove one at a time until   *
    ** has finished executing.                            */

    /* helpful to add print statements that explain whats  *
    ** being pushed on the stack and what's popped off    */

    public static  int recursiveFactorial(int num)
    {
        //If num = 1, result is 1
        //This is the breaking condition or base case, needed to end recursion
        if (num == 0 )
        {
            return 1;
        }

        //Otherwise call method recursively and return 1 less than current num
        System.out.println("Recursive Call: " + num);
        return num * recursiveFactorial(num - 1);

    }

    //Main method
    public static void main(String[] args)
    {
        //Call iterativeFactorial
        System.out.println("Iterative Factorial: " + iterativeFactorial(8));

        //Call recursiveFactorial
        System.out.println("Recursive Factorial: " + recursiveFactorial(8));

    }
}
