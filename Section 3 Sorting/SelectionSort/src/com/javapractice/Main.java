/* Course            : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 3        : Sorting Algorithms - Selection Sort         `        *
 ** Project Name     : SelectionSort                                        *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 12/30/2020                                           *
 ** Developer        : Tom Bielawski                                       */

package com.javapractice;

//Class
public class Main
{
    //Main method
    public static void main(String[] args)
    {
        //Declare and initialize array to sort
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        //Print unsorted array
        System.out.println("The unsorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }

        //Selection ort the array
        for (int lastUnsortedIndex = intArray.length - 1;
             lastUnsortedIndex > 0; lastUnsortedIndex-- )
        {
            //Declare an int to hold the largest value
            int largestElement = 0;

            //Is element at [1] greater than the largest [0]
            for (int i = 1; i <= lastUnsortedIndex; i++)
            {
                //If [i] is > than largest
                if (intArray[i] > intArray[largestElement])
                {
                    //Set i to largest
                    largestElement = i; 
                }

                //Call swap method
                swap(intArray, largestElement, lastUnsortedIndex);
            }

        }


        //For loop to print the Selection Sorted array
        System.out.println("\n\nThe selection sorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }

    }



    //Create a swap method to swap elements
    public static void swap (int[] array, int i, int j)
    {
        //Check if i = j. If so nothing to swap, move on.
        if (i == j) {return;}

        //Temporary int variable
        int temp = array[i];

        //Assign value at array element j to array element i
        array[i] = array[j];

        //Assign temp to array[j]
        array[j] = temp;

    }
}
