/*  Course           : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 3        : Sorting Algorithms - Shell Sort                      *
 ** Project Name     : ShellSort  - unstable                                *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 01/07/2021                                           *
 ** Developer        : Tom Bielawski                                        *
 ** Time Complexity  : Depends                                             */

/* This is an insertion sort with preliminary shifting                      */

package com.javapractice;

public class Main
{

    //Main method
    public static void main(String[] args)
    {
        //Declare and initialize array to sort
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        //Print unsorted array
        System.out.println("The unsorted array is: ");
        for (int i = 0; i < intArray.length; i++)
        {
            System.out.print(intArray[i] + ", ");
        }

        //Initialize gap, divide length of array by 2
        //Each iteration divides gap value by 2
        //Final gap must be greater than 0
        for (int gap = intArray.length / 2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < intArray.length; i++)
            {
                int newElement = intArray[i];

                //Use j to traverse array
                int j = i;

                //Once j becomes less than gap, the front of array is reached
                while (j >= gap && intArray[ j - gap] > newElement)
                {
                    //Shift element at j - gap up by number of gap positions
                    intArray[j] = intArray[j - gap];

                    //Drop out of loop
                    j-= gap;
                }

                intArray[j] = newElement;
            }

        }


        //For loop to print the Selection Sorted array
        System.out.println("\n\nThe selection sorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }
    }
}