/* Course            : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 3        : Sorting Algorithms - Bubble Sort         `           *
 ** Project Name     : SortingAlgorithms                                    *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 12/30/2020                                           *
 ** Developer        : Tom Bielawski                                       */

package com.javapractice;

//Class
public class Main
{

    //Main method
    public static void main(String[] args)
    {
        //Declare and initialize array to sort
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        //Print unsorted array
        System.out.println("The unsorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }
        System.out.println("\n\nThe sorted array is: ");

        //Use bubble algorithm : sort to the right
        //Decrement after each transversal, making sort shorter each time
        for (int lastUnsortedIndex = intArray.length - 1;
             lastUnsortedIndex > 0; lastUnsortedIndex-- )
        {
            //Bubble largest value to end of array
            for (int i = 0; i < lastUnsortedIndex; i++)
            {
                //Compare value of i vs i + 1. If i is greater, swap
                if (intArray[i] > intArray[i + 1])
                {
                    //Call swap method, pass intArray, i, and i + 1
                    swap(intArray, i, i + 1);
                }
            }

        }

        //For loop to print the Bubble Sorted array
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }

        /* Reverse bubble */
        for (int lastUnsortedIndex = intArray.length - 1;
             lastUnsortedIndex > 0; lastUnsortedIndex-- )
        {
            //Bubble largest value to end of array
            for (int i = 0; i < lastUnsortedIndex; i++)
            {
                //Compare value of i vs i + 1. If i is greater, swap
                if (intArray[i] < intArray[i + 1])
                {
                    //Call swap method, pass intArray, i, and i + 1
                    swap(intArray, i, i + 1);
                }
            }

        }


        //for loop to print reverse sorted array
        System.out.println("\n\nThe reverse sorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }
    }

    //Create a swap method to swap elements
    public static void swap (int[] array, int i, int j)
    {
        //Check if i = j. If so nothing to swap, move on.
        if (i == j) {return;}

        //Temporary int variable
        int temp = array[i];

        //Assign value at array element j to array element i
        array[i] = array[j];

        //Assign temp to array[j]
        array[j] = temp;

    }
}
