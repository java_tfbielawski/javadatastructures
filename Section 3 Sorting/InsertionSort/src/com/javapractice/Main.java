/* Course            : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 3        : Insertion Sort (In Place Algorithm)     `            *
 ** Project Name     : InsertionSort                                        *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 1/1/2021                                             *
 ** Developer        : Tom Bielawski                                       */

package com.javapractice;

public class Main
{

    //Main method
    public static void main(String[] args)
    {
        //Declare and initialize array to sort
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};
        System.out.println("The unsorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }
        System.out.println("");

        //For Loop to iterate the array
        for (int firstUnsortedIndex = 0; firstUnsortedIndex < intArray.length;
             firstUnsortedIndex++)
        {
            //
            int newElement = intArray[firstUnsortedIndex];

            //
            int temp = 0;

            //
            for (temp = firstUnsortedIndex; temp > 0 && intArray[temp - 1] > newElement; temp--)
            {
                //
                intArray[temp] = intArray[temp - 1];       
                System.out.println("\ntemp is: " + temp);
                System.out.println("intArray[temp] is: " + intArray[temp]);
                System.out.println("temp - 1 is: " + intArray[temp - 1]);
                System.out.println("new element is: " + newElement);
                System.out.println("firstUnsortedIndex is " + firstUnsortedIndex);

                //
                System.out.print("\nThe sorted array so far is:\n");
                for (int j = 0; j < intArray.length; j++ )
                {
                    System.out.print( intArray[j] + ", ");
                }
                //
                System.out.println("");

            }
            //
            System.out.println("");
            //
            intArray[temp] = newElement;
        }

        //For loop to print the Insertion Sorted array
        System.out.println("\n\nThe insertion sorted array is: ");
        for (int i = 0; i < intArray.length; i++ )
        {
            System.out.print(intArray[i] + ", ");
        }
    }
}
