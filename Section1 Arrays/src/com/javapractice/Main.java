/* Course            : Data Structures and Algorithms: Deep Dive Using Java *
 ** Section 1        : Arrays and Big O Notation  `         `               *
 ** Project Name     : Arrays.Java                                          *
 ** Course Instructor: @SarahEttrich                                        *
 ** Date             : 12/24/2020                                           *
 ** Developer        : Tom Bielawski                                       */

package com.javapractice;


public class Main
{
    //Main method
    public static void main(String[] args)
    {
        /* Basic array: O(n) Linear Time */

        //Declare a new int array of size 7
        //Hypothetical start address of 12
        //x + i * y = memory address of subsequent elements
        int[] intArray = new int[7];

        //Initialize first element
        //Address [0] = 12 + (0 * 4) = 12
        intArray[0] = 20;

        //Initialize second element
        //Address [1]: 12 + (1 * 4) = 16
        intArray[1] = 35;

        //Initialize third element
        //Address [2]: 12 + (2 * 4) = 20
        intArray[2] = -15;

        //Initialize fourth element
        //Address [3]: 12 + (3 * 4) = 24
        intArray[3] = 7;

        //Initialize fifth element
        //Address [4]: 12 + (4 * 4) = 28
        intArray[4] = 55;

        //Initialize sixth element
        //Address [5]: 12 + (5 * 4) = 32
        intArray[5] = 1;

        //Initialize seventh element
        //Address [6]: 12 + (6 * 4) = 36
        intArray[6] = -22;

        //For loop to iterate array
        for (int i = 0; i < intArray.length; i++)
        {
            //Print the value of the current index
            System.out.println(intArray[i]);
        }

        //Find the element that holds the value of 7
        //Declare a variable to hold the element when it's found
        int index = 0;
        for (int i = 0; i < intArray.length; i++)
        {
            //Loop stops when i = desired value of 7
            if (intArray[i] == 7)
            {
                //Assign i to index
                index = i;
                break;
            }
        }

        //Display the element that holds the desired value of 7
        System.out.println("index = " + index);

        /*Big O: n = 7 because there are 7 elements in the array    *
        **Number of steps is dependent upon the number of elements  *
        **This is linear O(n) because index isn't known             *
        **Location is not known, entire array may need to be looped */

        /* Adding to end of an array: O(1) Constant time*/

    }

}
